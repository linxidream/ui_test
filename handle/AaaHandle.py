import GlobalParam as gb
import time
from utils.Logger import Logger


class AaaHandle:

    def __init__(self, kw):
        self.kw=kw

    def inputSelect(self,data,locate_data):
        # 访问百度
        self.kw.visitWeb(gb.base_url)
        # 输入查询项
        self.kw.inputByXPath(locate_data['name'], data['input_name'])
        # 点击查询
        self.kw.click(locate_data['search_button'])

        # 等待结果
        time.sleep(1)
        xpath=gb.get_result_xpath(data, 'xpath')
        self.kw.visibleWait(xpath)
        msg=self.kw.driver.find_element_by_xpath(xpath).text
        Logger.infoLog('actual:%s'%msg)
        assert msg==data['expected']
