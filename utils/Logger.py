import logging
import os
import time
import GlobalParam as gb

class Logger(object):
    cur_date = time.strftime("%Y-%m-%d", time.localtime())
    LOG_FORMAT = "%(asctime)s - %(levelname)s: %(message)s"
    log_path = gb.log_path
    if not os.path.exists(log_path):
        os.makedirs(log_path)
    filename = os.path.join(log_path,gb.log_name.format(cur_date))
    logging.basicConfig(filename=filename, level=logging.DEBUG,
                        format=LOG_FORMAT)

    # 静态方法
    @staticmethod
    def debugLog(*args):
        logging.debug(*args)

    @staticmethod
    def infoLog(*args):
        logging.info(*args)

    @staticmethod
    def warLog(*args):
        logging.warning(*args)

    @staticmethod
    def errLog(*args):
        logging.error(*args)

    @staticmethod
    def crtLog(*args):
        logging.critical(*args)