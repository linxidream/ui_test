#!/usr/bin/env python 
# encoding: utf-8 

import json


class AssertHandler:
    def __init__(self):
        pass

    @staticmethod
    def json_compare(exp, act):
        # 先把exp、act转成json格式
        exp_json = None
        act_json = None
        if not isinstance(exp, dict):
            exp_json = json.loads(exp)
        else:
            exp_json = exp
        if not isinstance(act, dict):
            act_json = json.loads(act)
        else:
            act_json = act

        keys = exp_json.keys()
        act_keys = act_json.keys()
        for key in keys:
            if key in act_keys:
                if str(act_json[key]) != str(exp_json[key]):
                    return False
        return True


    @classmethod
    def assert_full_text_search(cls, context, act, count):
        """
        全文检索
        :count: 0--没有符合条件的查询  大于0--有符合条件的查询
        :return: True/False
        """
        # 判断code=100000
        is_pass = cls.json_compare({"code": 100000, "message": "成功"}, act)
        if not is_pass:
            return is_pass

        result = cls.get_result(act)
        if count > 0:
            return cls.full_text_search(context, result)
        else:
            # 判断len(result) < 1
            if len(result) < 1:
                return True
            else:
                return False

    @classmethod
    def full_text_search(cls, context, result):
        """
        全文检索:判断result各元素的值是否存在context
        :param context: string or 数字
        :param result: [{}, {}]
        :return: True/False
        """
        is_pass = True
        # 判断result中各元素
        for item in result:
            is_pass = cls.object_contains(context, item)
            if not is_pass:
                return is_pass

        return is_pass

    @classmethod
    def object_contains(cls, context, object):
        keys = object.keys()
        for key in keys:
            # 将value转换为字符串
            value = object[key]
            if not isinstance(value, str):
                value = str(value)
            # 判断value中是否包含context
            if context in value:
                return True

        return False


    @classmethod
    def assert_field_search(cls, exp, act, type, count):
        """
        根据字段搜索断言
        :type: like/equals
        :count: 0--没有符合条件的查询  大于0--有符合条件的查询
        :return: True/False
        """
        # 判断code==100000
        is_pass = cls.json_compare({"code": 100000, "message": "成功"}, act)
        if not is_pass:
            return is_pass

        result = cls.get_result(act)
        if count > 0:
            return cls.search_compare_equal(exp, result, type)
        else:
            # 判断len(result) < 1
            if len(result) < 1:
                return True
            else:
                return False

    @classmethod
    def search_compare_equal(cls, exp, result, type):
        """
        根据字段搜索:判断result中各元素的值是否正确
        :param exp: {"key1":"value1", "key2": "value2"}
        :param result: [{},{}]
        :return: True/False
        """
        is_pass = True
        # 判断result中各元素
        regulations = exp.keys()
        if type is "equals":  # item[key1]=value1， item[key2]=value2
            for key in regulations:
                is_pass = cls.compare_key_equals(result, key, exp[key])
                if not is_pass:
                    return is_pass
        else:  # item[key1] 包含 value1， item[key2] 包含 value2
            for key in regulations:
                is_pass = cls.compare_key_like(result, key, exp[key])
                if not is_pass:
                    return is_pass

        return is_pass

    @classmethod
    def compare_key_equals(cls, result, key, value):
        """
        判断list中每个元素的item[key] = value
        """
        for item in result:
            if key in item.keys():  # item中存在key，则判断item[key]是否等于value
                if item[key] != value:
                    return False
            else:  # item没有key，则直接返回False
                return False
        return True

    @classmethod
    def compare_key_like(cls, list, key, value):
        """
        判断list中每个元素的item[key] 包含 value
        """
        for item in list:
            if key in item.keys():  # item中存在key，则判断item[key]是否包含value
                if value not in item[key]:
                    return False
            else:  # item没有key，则直接返回False
                return False
        return True

    @classmethod
    def get_result(cls, response):
        result = None
        if not isinstance(response, dict):
            result = json.loads(response)['result']
        else:
            result = response['result']

        return result
