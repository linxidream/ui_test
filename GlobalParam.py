log_path="c:\cpc\log"
log_name="bb_{}.log"

base_url="https://baidu.com"

# 操作成功/失败提示语xpath
DEFAULT_RESULT_XPATH = "//span[@class='layui-layer-setwin']/preceding-sibling::div"

def get_result_xpath(param_dir, key):
    if key in param_dir.keys():
        return param_dir[key]
    else:
        return DEFAULT_RESULT_XPATH