import sys
import unittest


class Test(unittest.TestCase):
    def test_aaa(self):
        print("11111")
        assert 1==2

    def test_bbb(self):
        print("2222")
        pass

    def test_ccc(self):
        print("3333")


if __name__=='__main__':
    # 运行单个用例
    testcase=Test("test_bbb")
    unittest.TextTestRunner().run(testcase)

    #运行类里的用例
    testcase=unittest.TestLoader().loadTestsFromName('Test')
    unittest.TextTestRunner().run(testcase)
