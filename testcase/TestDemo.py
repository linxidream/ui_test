import unittest
import time
from ddt import data, ddt
from common.KeyWordOfWeb import KeyWordOfWeb
from handle.AaaHandle import AaaHandle
from param import AaaData


@ddt
class TestDemo(unittest.TestCase):

    def setUp(self):
        self.kw=KeyWordOfWeb()
        self.kw.openBrowser('Chrome')

    @data(AaaData.aaa_info)
    def test_aaa(self, data):
        """demo 单个参数 """
        aaa=AaaHandle(self.kw)
        aaa.inputSelect(data, AaaData.aaa_page)

    @data(*AaaData.bbb_info)
    def test_bbb(self, data):
        """demo  多个参数"""
        aaa=AaaHandle(self.kw)
        aaa.inputSelect(data, AaaData.aaa_page)

    def tearDown(self):
        time.sleep(1)
        self.kw.closeBrowser()


if __name__=='__main__':
    # 测试用例运行单个用例
    testcase=TestDemo("test_aaa_1")
    unittest.TextTestRunner().run(testcase)

    # # 测试用例运行类里的用例
    # testcase=unittest.TestLoader().loadTestsFromName('TestDemo')
    # unittest.TextTestRunner().run(testcase)