# 登录信息
aaa_info={
    "input_name": "测试测试测试测试",
    "expected": "百度为您找到相关结果约6,780,000个",
    "xpath": "//*[@id='container']/div[2]/div/div[2]/span",
    "url": ""
}

bbb_info=[
    {
        "input_name": "测试测试测试测试",
        "expected": "百度为您找到相关结果约6,780,000个",
        "xpath": "//*[@id='container']/div[2]/div/div[2]/span",
        "url": ""
    },
    {
        "input_name": "demodemodemodemo",
        "expected": "百度为您找到相关结果约75个",
        "xpath": "//*[@id='container']/div[2]/div/div[2]/span",
        "url": ""
    }
]

# 页面元素
aaa_page={
    "name": "//*[@id='kw']",
    "search_button": "//*[@id='su']"
}
