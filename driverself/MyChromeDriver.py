# !/use/bin/env python
# -*- coding:utf-8 -*-
import time

from selenium import webdriver
import os

"""
@author   : HuXiaoYu
@time     : 2019/8/28
@file     : MyChromeDriver.py
@describe :
"""


class MyChromeDriver:
    __driver=None

    def __init__(self):
        # os.makedirs('cache', exist_ok=True)
        options=webdriver.ChromeOptions()
        options.add_argument('disable-infobars')
        options.add_argument('--disable-gpu')
        options.add_argument("disable-extensions")
        # userdir = os.environ['USERPROFILE'] + "\\AppData\\Local\\Google\\Chrome\\User Data"
        # options.add_argument('--user-data-dir='+userdir)

        driverpath=os.path.join(os.path.dirname(__file__), "driverlib\chromedriver.exe")
        self.__driver=webdriver.Chrome(executable_path=driverpath, options=options)
        # self.__driver.get("https://mail.shunwang.com/")

    def getDriver(self):
        return self.__driver


if __name__=="__main__":
    chrome=MyChromeDriver()
    brose=chrome.getDriver()
    brose.delete_all_cookies()
    brose.quit()
