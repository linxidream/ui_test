# !/use/bin/env python
# -*- coding:utf-8 -*-

from selenium import webdriver
import os

"""
@author: huxy
@file: MyFirefoxDriver.py
@describe：
@time: 2019/8/29
"""


class MyFirefoxDriver:
    __driver = None

    def __init__(self):
        driverpath = os.path.join(os.path.dirname(__file__), "driverlib\geckodriver.exe")
        self.__driver = webdriver.Firefox(executable_path=driverpath)
        # self.__driver.get("http://www.baidu.com")


    def getDriver(self):
        return self.__driver


if __name__ == "__main__":
    chrome = MyFirefoxDriver()
    brose = chrome.getDriver()