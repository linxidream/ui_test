# !/use/bin/env python
# -*- coding:utf-8 -*-

from selenium import webdriver
import os

"""
@author: huxy
@file: MyIEDriver.py
@describe：
@time: 2019/8/29
"""


class MyIEDriver:
    __driver = None

    def __init__(self):
        driverpath = os.path.join(os.path.dirname(__file__), "driverlib\IEDriver.exe")
        self.__driver = webdriver.Ie(executable_path=driverpath)
        # self.__driver.get("http://www.baidu.com")

    def getDriver(self):
        return self.__driver


if __name__ == "__main__":
    chrome = MyIEDriver()
    brose = chrome.getDriver()