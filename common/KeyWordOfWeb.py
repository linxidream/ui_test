# !/use/bin/env python
# -*- coding:utf-8 -*-

from driverself.MyChromeDriver import *
from driverself.MyFirefoxDriver import *
from driverself.MyIEDriver import *
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import time
from common.ProcessOperation import AutoKillAllProcessNoQuestion
import GlobalParam as gb


class KeyWordOfWeb:
    driver=None

    def __init__(self):
        pass

    def openBrowser(self, browserType):
        # AutoKillAllProcessNoQuestion('chrome.exe')

        """
        打开浏览器
        :param browserType: 打开浏览器的类型，Chrome:谷歌；Firefox:火狐；IE:IE
        """
        if browserType=="Chrome":
            chrome=MyChromeDriver()
            self.driver=chrome.getDriver()
        elif browserType=="FireFox":
            firefox=MyFirefoxDriver()
            self.driver=firefox.getDriver()
        elif browserType=="IE":
            ie=MyIEDriver()
            self.driver=ie.getDriver()
        else:
            chrome=MyChromeDriver()
            self.driver=chrome.getDriver()

        # 浏览器网页最大化
        self.driver.maximize_window()
        self.driver.implicitly_wait(1)

    def visitWeb(self, url):
        """
        完成网页访问的方法
        :param url: 网址
        """
        if self.driver.title=='':
            self.driver.get(url)
        elif len(self.driver.window_handles)>0:
            # 打开新窗口
            js='window.open("'+url+'");'
            self.driver.execute_script(js)
            # 将窗口切换到新打开的页面
            handles=self.driver.window_handles
            self.driver.switch_to_window(handles[len(handles)-1])
        else:
            self.driver.get(url)
            # 将窗口切换到新打开的页面
            handles=self.driver.window_handles
            self.driver.switch_to_window(handles[len(handles)-1])

    def findElementByXPath(self, xpath):
        return self.driver.find_element_by_xpath(xpath)

    def findElementsByXPath(self, xpath):
        return self.driver.find_elements_by_xpath(xpath)

    def inputByName(self, name, content):
        """
        根据name属性，清空输入框并且输入内容的方法
        :param name: 定位用到的name属性
        :param content: 输入的内容
        """
        ele=self.driver.find_element_by_name(name)
        ele.clear()
        ele.send_keys(content)

    def inputByXPath(self, xpath, content):
        """
        根据xpath，清空输入框并且输入内容
        :param xpath: 定位用到的xpath
        :param content:输入的内容
        """
        self.visibleWait(xpath)
        ele=self.driver.find_element_by_xpath(xpath)
        time.sleep(1)
        self.driver.execute_script("arguments[0].value = \"\";", ele)
        ele.send_keys(content)

    def inputWithoutClear(self, xpath, content):
        """
        根据xpath在文本框中输入
        """
        ele=self.click(xpath)

        # self.driver.execute_script("arguments[0].value = arguments[1]", ele, content)
        ele.send_keys(content)

    def click(self, xpath):
        """
        点击按钮
        :param xpath: 定位用到的xpath
        """
        self.visibleWait(xpath)
        ele=None
        for i in range(0, 5):
            try:
                ele=self.driver.find_element_by_xpath(xpath)
                if ele!=None:
                    ele.click()
                    break
                else:
                    time.sleep(0.1)
            except Exception as e:
                time.sleep(0.1)

        return ele

    def visibleWait(self, xpath):
        """
        显示等待【最长等待20秒。每隔0.1秒检查一次是否出现xpath表示的元素,如果出现结束等待;若一直未出现,10秒后结束等待】
        :param xpath: 定位元素用到的xpath
        """
        try:
            # LOG.info('开始显示等待：' + xpath)
            WebDriverWait(self.driver, 20, 0.1).until(EC.presence_of_element_located((By.XPATH, xpath)), "未找到xpath元素")
            # LOG.info('结束显示等待:' + xpath)
        except Exception as e:
            pass

    def invisibleWait(self):
        """
        隐示等待【最长等待20。若20秒内页面加载完成，加载完成立即结束等待;若加载超过20秒，在20秒后结束等待】
        """
        self.driver.implicitly_wait(20)

    def closeBrowser(self):
        """
        关闭浏览器
        """
        self.driver.delete_all_cookies()
        self.driver.quit()

    def hover(self, xpath):
        """
        鼠标悬停在xpath表示的元素上
        :param xpath: 元素定位用到的xpath
        """
        ele=self.driver.find_element_by_xpath(xpath)
        ActionChains(self.driver).move_to_element(ele).perform()

    def switchWindow(self, pageTitle, close=False):
        """
        通过页面标题切换到目标窗口
        :param pageTitle: 目标窗口的页面标题
        :param close: 切换到目标窗口之前，是否关闭当前窗口
        """
        # 关闭当前窗口（根据close值断定是否关闭）
        if close:
            self.driver.close()

        # 切换到目标窗口：
        # 1.拿到所有窗口句柄
        # 2.按句柄循环切换窗口，切换一次判断一下是否切换到了目标窗口，不是继续切换，是就停止
        all_handles=self.driver.window_handles
        target_handles=None
        for handle in all_handles:
            self.driver.switch_to_window(handle)
            if self.driver.title==pageTitle:
                return

    def intoIframeByXpath(self, xpath):
        """
        通过xpath切换到iframe
        :param xpath: iframe的xpath
        """
        iframe=self.driver.find_element_by_xpath(xpath)
        self.driver.switch_to.frame(iframe)

    def intoIframeByName(self, name):
        """
        通过name切换到iframe
        :param name: iframe的name
        """
        self.driver.switch_to.frame(name)

    def outIframe(self):
        """
        跳出当前iframe
        """
        self.driver.switch_to.parent_frame()

    def bottomIframe(self):
        """
        返回最外层的iframe
        """
        self.driver.switch_to.default_content()

    def scrollWindowStraight(self, height):
        """
        浏览器上下滚动
        :param height:
        """
        jsCmd="window.scrollTo(0,"+height+")"
        self.driver.execute_script(jsCmd)

    def scrollWindow(self, xpath):
        """
        滚动至元素可见
        """
        ele=self.driver.find_element_by_xpath(xpath)
        self.driver.execute_script("arguments[0].scrollIntoView();", ele)
        time.sleep(1)

    def isPageContains(self, target):
        """
        断言网页中包含什么元素
        :param target:
        """
        pageContent=self.driver.page_source
        if target in pageContent:
            return True
        else:
            return False

    def screenShot(self, img_name):
        """
        截屏
        """
        pictureTime=time.strftime("%Y-%m-%d-%H_%M_%S", time.localtime(time.time()))
        directoryTime=time.strftime("%Y-%m-%d", time.localtime(time.time()))
        # 获取到当前文件的目录，并检查是否有 directory_time 文件夹，如果不存在则自动新建 directory_time 文件
        try:
            filePath=os.path.dirname(gb.log_path)+'\\log\\'+directoryTime+'\\'
            if not os.path.exists(filePath):
                os.makedirs(filePath)
        except BaseException as msg:
            print("新建目录失败：%s"%msg)

        self.driver.save_screenshot(filePath+img_name+'_'+pictureTime+'.png')

# if __name__ == '__main__':
#     kw = KeyWordOfWeb()
#     kw.isPageContains('百度一下')
