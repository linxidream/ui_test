import psutil
import os, signal
from tkinter import *  # 注意模块导入方式，否则代码会有差别，另见：import tkinter.messagebox 方法
from tkinter import messagebox

root=Tk()
root.withdraw()  # ****实现主窗口隐藏


# root.update() #*********需要update一下，不update也可以？


def judgeProcess(processname):
    '''
    判断相应的进程是否存在
    :param processname:
    :return:
    '''
    correctPid="-1";
    pl=psutil.pids()
    for pid in pl:
        if psutil.Process(pid).name()==processname:
            if correctPid=="-1":
                correctPid=str(pid)
            else:
                correctPid+=","+str(pid)

    return correctPid


def killProcess(pid):
    os.kill(pid, signal.SIGILL)


# 不经过问询直接关闭浏览器
def AutoKillAllProcessNoQuestion(processname):
    pids=judgeProcess(processname)
    if pids!="-1":
        arrPid=pids.split(",")
        for pid in arrPid:
            nPid=int(pid)
            killProcess(nPid)


# 经过问询关闭浏览器
def AutoKillAllProcess(processname):
    pids=judgeProcess(processname)
    isContinue=False  # 是否开启自动化脚本测试
    if pids!="-1":
        isContinue=messagebox.askokcancel(title='提示', message='是否关闭所有的chrome进程？')
        if isContinue:
            arrPid=pids.split(",")
            for pid in arrPid:
                nPid=int(pid)
                killProcess(nPid)
    else:
        isContinue=True

    return isContinue


if __name__=='__main__':
    AutoKillAllProcessNoQuestion('chrome.exe')
