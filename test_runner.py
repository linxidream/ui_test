import unittest
import os
from report.BeautifulReport import *

case_path=os.path.join(os.getcwd(), "testcase")


def CreatSuite():
    suite=unittest.TestSuite()
    all_case=unittest.defaultTestLoader.discover(case_path, pattern='Test*.py', top_level_dir=None)
    for test_case in all_case:
        suite.addTests(test_case)
    return suite


if __name__=="__main__":
    all_test=CreatSuite()
    # result = unittest.TestResult(all_test)
    # all_test.run(result)
    # print(result)
    result=BeautifulReport(all_test)
    result.report(filename='Report',
                  description='ui 测试报告',
                  log_path='report')
